using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Context;
using Pottencial.Models;

namespace Pottencial.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly PaymentContext _context;

        public PaymentController(PaymentContext context)
        {
            _context = context;
        }

        [HttpPost("RegistrarVenda")]
        public IActionResult RegistrarVenda(Vendedor vendedor, Item item)
        {
            _context.Add(vendedor, item);
            _context.SaveChanges();

            return CreatedAtAction(nameof(GetById), new { id = vendedor.Id}, vendedor);
        }

        [HttpGet("BuscarVenda/{id}")]
        public IActionResult BuscarVenda(int id)
        {
            var pedido = _context.Vendedores.Find(id);

            if(pedido == null)
                return NotFound();

            return Ok(pedido);
        }

        [HttpPut("AtualizarVenda{id}")]
        public IActionResult AtualizarVenda(int id, Item items)
        {
            var item = _context.Items.Find(id);

            if(item == null)
                return NotFound();
            
            item.Status = items.Status;

            _context.Items.Update(item);
            _context.SaveChanges();

            return Ok(item);
        }
    }
}