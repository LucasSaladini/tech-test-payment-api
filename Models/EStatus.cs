using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Pottencial.Models;

namespace Pottencial.Models
{
    public enum EnumStatus
    {
        Aguardando_pagamento,
        Pagamento_aprovado,
        Enviado_transportadora,
        Entregue,
        Cancelado
    }
}